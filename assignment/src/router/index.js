import Vue from "vue";
import Router from "vue-router";
import HelloWorld from "@/components/HelloWorld";
import findx from "@/components/findx.vue";
import placeAPI from "@/components/placeAPI.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "HelloWorld",
      component: HelloWorld
    },
    {
      path: "/test",
      name: "findx",
      component: findx
    },
    {
      path: "/place",
      name: "placeAPI",
      component: placeAPI
    }
  ]
});
