var express = require("express");
var router = express.Router();

const axios = require("axios");

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/place/:name", function(req, res, next) {
  // get lat,long
  axios
    .get(
      "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key=AIzaSyASwN6ZKEznAMnAS5k30-lrNmVlfsC0J_0&inputtype=textquery&input=" +
        req.params.name +
        "&fields=photos,formatted_address,name,rating,opening_hours,geometry"
    )
    .then(result => {
      var lat = result.data.candidates[0].geometry.location.lat;
      var long = result.data.candidates[0].geometry.location.lng;

      // get near restaurant
      axios
        .get(
          "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
            lat +
            "," +
            long +
            "&radius=3000&type=restaurant&keyword=cruise&key=AIzaSyASwN6ZKEznAMnAS5k30-lrNmVlfsC0J_0"
        )
        .then(result1 => {
          res.send(result1.data);
        })
        .catch(err => {
          res.send(err);
        });
    })
    .catch(err1 => {
      res.send(err1);
    });
});

module.exports = router;
